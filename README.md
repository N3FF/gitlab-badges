This is to add different color Gitlab logos to your profile depending on your contribution count.

- 1-99 - Grey Gitlab logo
- 100-499 Blue Gitlab logo
- 500-999 Green Gitlab logo
- 1000-1999 Red Gitlab logo
- 2000+ Gold Gitlab logo


- This will be displayed on the right side of their username when viewing their profile.
